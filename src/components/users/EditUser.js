import React, { useState, useEffect } from "react";
import axios from 'axios'
import { useHistory, useParams } from "react-router-dom";

const EditUser = (props) => {
  let history = useHistory();

  const { id } = useParams();
  const [user, setUser] = useState({});

  const onInputChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  const onSubmit = async event => {
    event.preventDefault();
    await axios.put(`${process.env.REACT_APP_BASE_URL}/employees/${id}`, user);
    history.push("/");
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_BASE_URL}/employees/${props.match.params.id}`)
      .then(res => res.json())
      .then((result) => {
        setUser(result);
      })
  }, [])

  return (
    <div className="container">
      <div className="w-75 mx-auto shadow p-5">
        <h2 className="text-center mb-4">Edit A User</h2>
        <form onSubmit={event => onSubmit(event)}>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter Your Name"
              name="name"
              value={user.name}
              onChange={event => onInputChange(event)}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter Your age"
              name="age"
              value={user.age}
              onChange={event => onInputChange(event)}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter Your Salary"
              name="salary"
              value={user.salary}
              onChange={event => onInputChange(event)}
            />
          </div>
          <button className="btn btn-primary btn-block">EditUser</button>
        </form>
      </div>
    </div>
  );
};

export default EditUser;